package ee.piiritaja.sotkanalyser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StockAnalyserApplication {

    public static void main(String[] args) {
        SpringApplication.run(StockAnalyserApplication.class, args);
    }

}
