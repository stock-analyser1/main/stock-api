package ee.piiritaja.sotkanalyser.webscraper;

import org.springframework.scheduling.annotation.Async;

import java.util.concurrent.CompletableFuture;

public abstract class Scraper {

    private final String exchange;
    private final String url;

    public Scraper(String exchange, String url) {
        this.exchange = exchange;
        this.url = url;
    }

    public Float getLatestPrice(String symbol) {
        return 0.0f;
    }

    public boolean isAvailable(String symbol) {
        return false;
    }

    @Async
    public CompletableFuture<String> getIdentifier(String symbol) {
        return CompletableFuture.completedFuture("");
    }

    public String getUrl() {
        return url;
    }

    public float getChange(String symbol){
        return 0.0f;
    }

    public String getExchange() {
        return exchange;
    }
}
