package ee.piiritaja.sotkanalyser.webscraper;

import com.google.gson.Gson;
import ee.piiritaja.sotkanalyser.exception.StockNotFoundException;
import ee.piiritaja.sotkanalyser.model.gsonObjects.exchange.baltic.BalticStock;
import ee.piiritaja.sotkanalyser.model.gsonObjects.exchange.baltic.Equity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CompletableFuture;

public class BalticScraper extends Scraper {

    public BalticScraper(String exchange, String url) {
        super(exchange, url);
    }

    private BalticStock getData() {
        StringBuilder json = new StringBuilder();
        try {
            URL url = new URL("https://nasdaqbaltic.com/market/ws/ticker");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
            String line = "";
            while ((line = bufferedReader.readLine()) != null) {
                json.append(line);
            }
        } catch (IOException ignore) {
        }
        return new Gson().fromJson(json.toString(), BalticStock.class);
    }

    private List<Equity> getAllStocks() {
        return getData().getEquities();
    }

    private Equity getStockBySymbol(String symbol) {
        return getAllStocks().stream().filter(equity -> equity.getName().equals(symbol.toUpperCase(Locale.ROOT))).findFirst().orElseThrow(StockNotFoundException::new);
    }

    @Override
    public CompletableFuture<String> getIdentifier(String symbol) {
        return CompletableFuture.completedFuture(getStockBySymbol(symbol).getIsin());
    }

    @Override
    public Float getLatestPrice(String symbol) {
        return Float.parseFloat(getStockBySymbol(symbol).getLast());
    }

    @Override
    public float getChange(String symbol) {
        return Float.parseFloat(getStockBySymbol(symbol).getChg());
    }
}
