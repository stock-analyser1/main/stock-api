package ee.piiritaja.sotkanalyser.service;

import ee.piiritaja.sotkanalyser.exception.StockNotFoundException;
import ee.piiritaja.sotkanalyser.model.stock.Stock;
import ee.piiritaja.sotkanalyser.repository.StockRepository;
import ee.piiritaja.sotkanalyser.webscraper.Scraper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;

@Service
public class StockService {

    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private Scraper balticScraper;

    public List<Stock> getAll() {
        return stockRepository.findAll();
    }

    public Stock getById(long id) {
        return stockRepository.findById(id).orElseThrow(StockNotFoundException::new);
    }

    public void deleteById(long id) {
        getById(id);
        stockRepository.deleteById(id);
    }

    public Stock save(Stock stock) {
        balticScraper.getIdentifier(stock.getSymbol()).thenApply(s -> {
            stock.setIsin(s);
            return stockRepository.save(stock);
        });
        return stockRepository.save(stock);
    }

    public Stock put(Stock stock, long id) {
        stockRepository.getById(id);
        stock.setId(id);
        return stockRepository.save(stock);
    }

    public void deleteAll() {
        stockRepository.deleteAll();
    }

    public Stock getSymbol(String symbol) {
        return stockRepository.getBySymbol(symbol.toUpperCase(Locale.ROOT));
    }
}
