package ee.piiritaja.sotkanalyser.service;

import ee.piiritaja.sotkanalyser.model.response.StockImageRequest;
import ee.piiritaja.sotkanalyser.model.stock.Stock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ImageService {
    private static final String UPLOAD_PATH = "storage/images/";

    @Autowired
    private StockService stockService;

    public File getImage(Stock stock) {
        List<File> files = getImages(stock.getSymbol());
        if (files.size() <= 0) {
            files = Arrays.stream(Objects.requireNonNull(new File(UPLOAD_PATH + "stocks/random").listFiles())).collect(Collectors.toList());
        }
        Random rand = new Random();
        return files.get(rand.nextInt(files.size()));
    }

    private List<File> getImages(String symbol) {
        File directory = new File(UPLOAD_PATH + "stocks/" + symbol.toLowerCase(Locale.ROOT));
        log.info("Getting images from " + directory.getAbsolutePath());
        if (!directory.exists()) {
            log.debug("Image directory does not exist. Returning random images.");
            return Arrays.stream(Objects.requireNonNull(new File(UPLOAD_PATH + "stocks/random").listFiles())).collect(Collectors.toList());
        }
        log.debug("Image directory exists. Returning images from directory.");
        return Arrays.stream(Objects.requireNonNull(new File(UPLOAD_PATH + "stocks/" + symbol)
                .listFiles())).collect(Collectors.toList());
    }

    public File getImageDirectory(long stockId) {
        String stockSymbol = stockService.getById(stockId).getSymbol();
        return new File(UPLOAD_PATH + "stocks/" + stockSymbol.toLowerCase(Locale.ROOT));
    }

    public void uploadStockImage(MultipartFile multipartFile, String stockSymbol) throws IOException {
        String extension = Objects.requireNonNull(multipartFile.getContentType()).split("/")[1];
        File directory = new File(UPLOAD_PATH + "stocks/" + stockSymbol.toLowerCase(Locale.ROOT));
        log.info("Uploading image to: " + directory.getPath());
        if (!directory.exists()) {
            log.debug("Path does not exist. Crating image path from stock.");
            directory.mkdir();
        }
        String fileName = UUID.randomUUID() + "." + extension;
        File convertedFile = new File(directory.getPath() + "/" + fileName.toLowerCase(Locale.ROOT));
        log.info("Converting file to: " + convertedFile.getName());
        if (convertedFile.createNewFile()) {
            log.debug("Created new file " + convertedFile.getPath());
        } else {
            log.debug("File already exists " + convertedFile.getPath());
        }
        if (Arrays.asList("png", "jpeg", "jpg").contains(extension)) {
            FileOutputStream fout = new FileOutputStream(convertedFile);
            fout.write(multipartFile.getBytes());
            log.info("Created image with size" + multipartFile.getSize() + " bytes.");
            fout.close();
        } else {
            log.error("Unsupported extension: " + extension);
            log.info("File not written.");
        }
    }

    @Async
    public CompletableFuture<ResponseEntity<?>> sendRequest(StockImageRequest stockImageRequest) throws InterruptedException {
        String url = "http://localhost:9091/images/stocks/";
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<?> response = ResponseEntity.notFound().build();

        try {
            response = restTemplate.postForEntity(url, stockImageRequest, Void.class);
        } catch (ResourceAccessException | HttpClientErrorException ignore) {

        }
        return CompletableFuture.completedFuture(response);
    }

}