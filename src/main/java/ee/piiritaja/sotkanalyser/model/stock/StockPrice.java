package ee.piiritaja.sotkanalyser.model.stock;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class StockPrice {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private Date priceDate;
    private float price;
}
