package ee.piiritaja.sotkanalyser.model.stock;

import javax.persistence.*;
import java.util.List;

@Entity
public class StockData {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @OneToMany
    private List<StockPrice> prices;
}
