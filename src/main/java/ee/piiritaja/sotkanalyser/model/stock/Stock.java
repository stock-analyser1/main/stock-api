package ee.piiritaja.sotkanalyser.model.stock;

import ee.piiritaja.sotkanalyser.model.request.StockRequest;
import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
@Getter
@Setter
public class Stock {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private long id;
    @Column(unique = true)
    private String symbol;
    private String name;
    @Column(columnDefinition = "TEXT")
    private String description;
    private String isin;
    @OneToOne
    private StockData stockData;

    public static Stock from(StockRequest stockRequest) {
        return Stock.builder().name(stockRequest.getName()).description(stockRequest.getDescription())
                .symbol(stockRequest.getSymbol()).build();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        Stock stock = (Stock) o;
        if (o == null || getClass() != o.getClass()) return false;

        return id == stock.id && symbol.equals(stock.symbol) && Objects.equals(name, stock.name) && Objects.equals(description, stock.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, symbol, name, description);
    }
}
