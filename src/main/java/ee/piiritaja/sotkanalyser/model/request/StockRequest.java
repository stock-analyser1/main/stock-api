package ee.piiritaja.sotkanalyser.model.request;

import lombok.Builder;
import lombok.Getter;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@Getter
@Builder
public class StockRequest {
    private final String name;
    private final String description;
    private final String symbol;
}
