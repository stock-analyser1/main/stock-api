package ee.piiritaja.sotkanalyser.model.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StockImageRequest {
    private long stockId;
    private String stockSymbol;
    private byte[] stockImage;
    private String imageType;
}
