package ee.piiritaja.sotkanalyser.model.gsonObjects.exchange.baltic;

import lombok.Getter;

import java.util.List;

@Getter
public class BalticStock {
    private List<Index> indexes;
    private List<Equity> equities;
}
