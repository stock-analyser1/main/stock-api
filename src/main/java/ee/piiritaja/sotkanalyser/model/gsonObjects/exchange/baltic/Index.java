package ee.piiritaja.sotkanalyser.model.gsonObjects.exchange.baltic;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Index {
    private String name;
    @SerializedName("short")
    private String shortId;
    private String last;
    private String chg;
}
