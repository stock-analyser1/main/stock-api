package ee.piiritaja.sotkanalyser.model.gsonObjects.exchange.baltic;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Equity {
    private String name;
    private String isin;
    private String chg;
    private String last;
    private String ccy;
}
