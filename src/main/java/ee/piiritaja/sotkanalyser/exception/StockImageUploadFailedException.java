package ee.piiritaja.sotkanalyser.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
public class StockImageUploadFailedException extends RuntimeException {
}
