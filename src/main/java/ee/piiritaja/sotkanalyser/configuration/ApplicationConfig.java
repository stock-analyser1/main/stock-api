package ee.piiritaja.sotkanalyser.configuration;

import ee.piiritaja.sotkanalyser.webscraper.BalticScraper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

@Configuration
@EnableAsync
public class ApplicationConfig {

    @Bean
    public BalticScraper balticScraper(){
        return new BalticScraper("NASDAQ","https://nasdaqbaltic.com/et/");
    }
}
