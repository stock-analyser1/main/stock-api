package ee.piiritaja.sotkanalyser.controller;

import ee.piiritaja.sotkanalyser.model.response.StockImageRequest;
import ee.piiritaja.sotkanalyser.model.stock.Stock;
import ee.piiritaja.sotkanalyser.service.ImageService;
import ee.piiritaja.sotkanalyser.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@RestController
@RequestMapping("images")
public class ImageController {

    private static final String IMAGE_PREFIX = "storage/";

    @Autowired
    ImageService imageService;

    @Autowired
    StockService stockService;

    @PostMapping("{symbol}")
    public void postImage(@RequestBody MultipartFile file, @PathVariable String symbol) throws IOException, InterruptedException {
        StockImageRequest stockImageRequest = StockImageRequest.builder().stockSymbol(symbol).stockImage(file.getBytes()).build();
        imageService.sendRequest(stockImageRequest);
    }

    @GetMapping("/stocks/{symbol}")
    public ResponseEntity<byte[]> getTaste(@PathVariable String symbol) throws IOException {
        Stock stock = stockService.getSymbol(symbol);
        if (stock == null) {
            return null;
        }
        File image = imageService.getImage(stock);
        byte[] bytes = Files.readAllBytes(Path.of(image.getPath()));
        return ResponseEntity
                .ok()
                .contentType(MediaType.IMAGE_JPEG)
                .body(bytes);
    }
}
