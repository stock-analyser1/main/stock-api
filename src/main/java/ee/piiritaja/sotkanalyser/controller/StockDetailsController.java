package ee.piiritaja.sotkanalyser.controller;

import ee.piiritaja.sotkanalyser.webscraper.Scraper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

@RestController
@RequestMapping("stocks/details")
public class StockDetailsController {

    @Autowired
    private Scraper balticScraper;

    @GetMapping("{symbol}/price/last")
    public float getLasPrice(@PathVariable String symbol) {
        return balticScraper.getLatestPrice(symbol);
    }

    @GetMapping("{symbol}/price/change")
    public float getChange(@PathVariable String symbol) {
        return balticScraper.getChange(symbol);
    }
}
