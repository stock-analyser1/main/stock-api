package ee.piiritaja.sotkanalyser.controller;

import ee.piiritaja.sotkanalyser.exception.StockImageUploadFailedException;
import ee.piiritaja.sotkanalyser.model.response.StockImageRequest;
import ee.piiritaja.sotkanalyser.model.stock.Stock;
import ee.piiritaja.sotkanalyser.model.request.StockRequest;
import ee.piiritaja.sotkanalyser.service.ImageService;
import ee.piiritaja.sotkanalyser.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
@RequestMapping("stocks")
public class StockController {

    @Autowired
    private StockService stockService;

    @Autowired
    private ImageService imageService;

    @GetMapping
    public List<Stock> getAll() {
        return stockService.getAll();
    }

    @GetMapping("{id}")
    public Stock getById(@PathVariable long id) {
        return stockService.getById(id);
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE})
    public Stock post(@RequestPart String name, @RequestPart String symbol, @RequestPart String description, @RequestPart MultipartFile image) throws IOException, ExecutionException, InterruptedException {
        StockRequest stock = StockRequest.builder().name(name).symbol(symbol).description(description).build();
        Stock stockResponse = stockService.save(Stock.from(stock));
        StockImageRequest stockImageRequest = StockImageRequest.builder()
                .stockImage(image.getBytes())
                .stockSymbol(symbol)
                .stockId(stockResponse.getId())
                .build();

        CompletableFuture<ResponseEntity<?>> imageRes = imageService.sendRequest(stockImageRequest);

        int code = imageRes.get().getStatusCode().value();
        if (code != 200){
            throw new StockImageUploadFailedException();
        }
        return stockResponse;
    }

    @PutMapping(value = "{id}")
    public Stock put(@PathVariable long id, @RequestBody StockRequest request) {
        Stock stock = Stock.from(request);
        return stockService.put(stock, id);
    }

    @DeleteMapping("{id}")
    public void deleteById(@PathVariable long id) {
        stockService.deleteById(id);
    }


}
