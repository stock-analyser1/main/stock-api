package ee.piiritaja.sotkanalyser.repository;

import ee.piiritaja.sotkanalyser.model.stock.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockRepository extends JpaRepository<Stock, Long> {

    Stock getBySymbol(String symbol);
}
