package ee.piiritaja.sotkanalyser.service;

import ee.piiritaja.sotkanalyser.exception.StockNotFoundException;
import ee.piiritaja.sotkanalyser.model.stock.Stock;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@ActiveProfiles("test")
public class StockServiceTest {

    @Autowired
    private StockService stockService;

    private Stock lhv = Stock.builder().name("LHV")
            .description("See on lhv")
            .symbol("LHV1T")
            .build();

    private Stock tallink = Stock.builder().name("Tallink Grupp")
            .symbol("TAL1T")
            .description("See on Tallink")
            .build();

    private Stock kaubamaja = Stock.builder().name("Tallinna Kaubamaja")
            .symbol("TKM1T")
            .description("See on Tallinna Kaubamaja")
            .build();

    private Stock sadam = Stock.builder().name("Tallinna Sadam")
            .symbol("TSM1T")
            .description("See on Tallinna Sadam")
            .build();

    private static final String LHV_ISIN = "EE3100073644";

    public void initializeStocks() {
        stockService.deleteAll();
        lhv = stockService.save(lhv);
        tallink = stockService.save(tallink);
        kaubamaja = stockService.save(kaubamaja);
        sadam = stockService.save(sadam);
    }

    @AfterEach
    public void clearStocks() {
        stockService.deleteAll();
    }

    @Test
    public void getAllStocksEmpty() {
        assertEquals(0, stockService.getAll().size());
    }

    @Test
    public void saveStocksAddsStock() {
        stockService.deleteAll();
        List<Stock> stocks = Arrays.asList(lhv, tallink, kaubamaja, sadam);
        int count = 0;
        for (Stock stock : stocks) {
            count += 1;
            stockService.save(stock);
            assertEquals(count, stockService.getAll().size());
        }
    }

    @Test
    public void saveStocksReturnsCorrectStock() {
        List<Stock> stocks = Arrays.asList(lhv, tallink, kaubamaja, sadam);
        for (Stock stock : stocks) {
            Stock newStock = stockService.save(stock);
            assertEquals(stock, newStock);
        }
    }

    @Test
    public void editStockCorrect() {
        initializeStocks();
        Stock stock = stockService.getAll().get(0);
        Stock modified = Stock.builder().name("Modified").description("This is a modified stock").symbol("rnd").build();
        stockService.put(modified, stock.getId());
        stock = stockService.getById(stock.getId());
        assertEquals(stock, modified);
    }

    @Test
    public void deleteStocksDeletesStocks() {
        initializeStocks();
        assertTrue(stockService.getAll().size() != 0);
        stockService.deleteAll();
        assertEquals(0, stockService.getAll().size());
    }

    @Test
    public void getByIdStockNotFoundThrowsException() {
        initializeStocks();
        long idNotAvailable = stockService.getAll().stream().mapToLong(Stock::getId).sum();
        assertThrows(StockNotFoundException.class, () -> {
            stockService.getById(idNotAvailable);
        });
    }

    @Test
    public void deleteByIdStockFoundDeletesStock() {
        initializeStocks();
        Stock stock = stockService.getAll().get(0);
        stockService.deleteById(stock.getId());
        assertFalse(stockService.getAll().contains(stock));
    }

    @Test
    public void deleteByIdStockNotFoundThrowsException() {
        initializeStocks();
        long idNotAvailable = stockService.getAll().stream().mapToLong(Stock::getId).sum();
        assertThrows(StockNotFoundException.class, () -> {
            stockService.deleteById(idNotAvailable);
        });
    }

}
