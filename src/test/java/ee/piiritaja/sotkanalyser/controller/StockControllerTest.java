package ee.piiritaja.sotkanalyser.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import ee.piiritaja.sotkanalyser.model.stock.Stock;
import ee.piiritaja.sotkanalyser.service.StockService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.*;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class StockControllerTest {


    @Autowired
    private MockMvc mvc;

    @Autowired
    private StockController controller;

    @Autowired
    private StockService stockService;

    private ObjectMapper mapper = new ObjectMapper();

    private ObjectWriter writer = mapper.writer().withDefaultPrettyPrinter();

    private Stock lhv;

    private Stock tallink;

    private Stock kaubamaja;

    private Stock sadam;

    @BeforeEach
    public void initializeStocks() {
        stockService.deleteAll();
        lhv = Stock.builder().name("LHV")
                .description("See on lhv")
                .symbol("LHV1T")
                .build();
        tallink = Stock.builder().name("Tallink Grupp")
                .symbol("TAL1T")
                .description("See on Tallink")
                .build();
        kaubamaja = Stock.builder().name("Tallinna Kaubamaja")
                .symbol("TKM1T")
                .description("See on Tallinna Kaubamaja")
                .build();
        sadam = Stock.builder().name("Tallinna Sadam")
                .symbol("TSM1T")
                .description("See on Tallinna Sadam")
                .build();
        lhv = stockService.save(lhv);
        tallink = stockService.save(tallink);
        kaubamaja = stockService.save(kaubamaja);
        sadam = stockService.save(sadam);

    }

    @Test
    public void contextLoads() {
        assertNotNull(controller);
    }

    @Test
    public void getAllStocksPathIsOk() throws Exception {
        this.mvc.perform(get("/stocks")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void getStockByIdPathIsOk() throws Exception {
        this.mvc.perform(get("/stocks/" + lhv.getId())).andExpect(status().isOk());
    }

    @Test
    public void getAllStocksReturnsCorrectStocks() throws Exception {
        MvcResult result = this.mvc.perform(get("/stocks")).andDo(print()).andExpect(status().isOk())
                .andReturn();
        List<Stock> res = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<Stock>>() {
        });
        assertArrayEquals(Stream.of(lhv, tallink, kaubamaja, sadam).mapToLong(Stock::getId).toArray(), res.stream().mapToLong(Stock::getId).toArray());
    }

    @Test
    public void getStockByIdReturnsCorrectStock() throws Exception {
        List<Stock> stocks = Arrays.asList(lhv, tallink, kaubamaja, sadam);
        Random rand = new Random();
        Stock chosenStock = stocks.get(rand.nextInt(stocks.size()));
        MvcResult result = this.mvc.perform(get("/stocks/" + chosenStock.getId())).andExpect(status().isOk())
                .andReturn();
        Stock res = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Stock>() {
        });
        assertEquals(res, chosenStock);
    }

    @Test
    public void postStockReturnsCorrectStock() throws Exception {
        Stock newStock = Stock.builder().name("random").build();
        String requestJson = writer.writeValueAsString(newStock);
        MvcResult result = this.mvc.perform(post("/stocks").contentType(MediaType.APPLICATION_JSON)
                .content(requestJson)).andExpect(status().isOk()).andReturn();
        Stock res = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Stock>() {
        });
        assertEquals("random", res.getName());
    }

    @Test
    public void postStockSavesStock() throws Exception {
        Stock newStock = Stock.builder().name("random").build();
        String requestJson = writer.writeValueAsString(newStock);
        MvcResult result = this.mvc.perform(post("/stocks").contentType(MediaType.APPLICATION_JSON)
                .content(requestJson)).andExpect(status().isOk()).andReturn();
        Stock res = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<Stock>() {
        });
        Stock dbRes = stockService.getById(res.getId());
        assertEquals(dbRes.getId(), res.getId());
    }

    @Test
    public void getStockWrongStockThrowsException() throws Exception {
        List<Stock> stocks = Arrays.asList(lhv, tallink, kaubamaja, sadam);
        long indexNotAvailable = stocks.stream().mapToLong(Stock::getId).sum();
        this.mvc.perform(get("/stocks/" + indexNotAvailable)).andExpect(status().isNotFound());
    }

}
