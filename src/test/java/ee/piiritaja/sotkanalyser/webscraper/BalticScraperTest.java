package ee.piiritaja.sotkanalyser.webscraper;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BalticScraperTest {

    private static final String LHV_ISIN = "EE3100073644";
    private static final String LHV_SYMBOL = "LHV1T";

    @Test
    void getIdentifier() {
        Scraper scraper = new BalticScraper("", "");
        scraper.getIdentifier(LHV_SYMBOL).thenApply(s -> {
            assertEquals(LHV_ISIN, s);
            return s;
        });
    }
}